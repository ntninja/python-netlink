# Python NetLink #

Python NetLink is a pythonic pure-Python API for the Linux NetLink set of APIs (and some related
ones where it makes sense). It aims to be as efficient as possible, while still giving you
high-level and easy-to-use access to most common operations.

When high-level access is not enough, Python NetLink also gives you the powerful low-level API
that can be used to assemble arbitrary NetLink and ETHTool commands, send them to the kernel and
parse the response.

## Documentation ##

Documentation is available [online](https://xmine128.tk/Software/Python/netlink/docs/)

## Releases ##

Releases are available on [PyPI](https://pypi.python.org/pypi/NetLink) and on the
[documentation server](https://xmine128.tk/Software/Python/netlink/dist/).

## Source code & Development ##

Source code is hosted on [the GitLab public server](https://gitlab.com/alexander255/python-netlink).
