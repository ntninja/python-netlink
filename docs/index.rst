.. NetLink documentation master file, created by
   sphinx-quickstart on Sat Dec 26 21:00:55 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python NetLink's documentation!
==========================================

Python NetLink is a pythonic pure-Python API for the Linux NetLink set of APIs (and some related
ones where it makes sense). It aims to be as efficient as possible, while still giving you
high-level and easy-to-use access to most common operations.

When high-level access is not enough, Python NetLink also gives you the powerful low-level API
that can be used to assemble arbitrary NetLink and ETHTool commands, send them to the kernel and
parse the response.

Contents
--------

.. toctree::
   :maxdepth: 2
   
   high-level



Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

