# High-Level API #

## Examples ##

### Changing the IP addresses of a network device ###

**Note:** This example requires effective UID `0` ("*root*") or `CAP_NET_ADMIN` privileges to work.

First of all, let's add a new IP address `2002:456:1::1/64` to the network interface `wlan0` and grant it a lifetime of
60 seconds:

```
import netlink

network = netlink.Network()
device  = network.device('wlan0')
device.add_address('2002:4BC:1::1/64', valid_lft = 60, preferred_lft = 30)
```

**Note:** If you get an error similar to `OSError: [Errno 19] No such device` you'll have to use the `ip address show`
command to determine the name of a usable network device (such as `eth0` or even `lo`) and use it instead of `wlan0`
in all example scripts.

Once we have successfully run the above code (either in an interactive Python shell or by copying it to a script file
and running it), we can verify that the address was actually created using the `ip address` command:

```sh
$ ip address show dev wlan0
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    …
    inet6 2002:4BC:1::1/64 scope global dynamic 
       valid_lft 40sec preferred_lft 10sec
    …
```

The output might look a bit different for you and (if the device is currently used) there'll probably be several other
addresses shown next to the one we have just added.

You'll also notice that the kernel already started counting down from the original 60 seconds we granted the address.
I'd be a shame if our address were to disappears in less than 40 seconds, so let's buy it some extra time using
[`modify_address`](#netlink.NetworkDevice.modify_address):

```
import netlink

network = netlink.Network()
device  = network.device('wlan0')
device.modify_address('2002:4BC:1::1/64', valid_lft = 2000, preferred_lft = 300)
```

```sh
$ ip address show dev wlan0
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    …
    inet6 2002:4BC:1::1/64 scope global dynamic 
       valid_lft 1990sec preferred_lft 290sec
    …
```

Excellent! Finally let's get rid of this address after all and add a different one instead:

```
import netlink

network = netlink.Network()
device  = network.device('wlan0')
device.add_address('fe90::223:14ff:feb9:384/64', scope = 'Link', valid_lft = 30, preferred_lft = 30)
device.remove_address('2002:4BC:1::1/64')
```

Since `fc00::/7` is the local IPv6 address range, we have also told to kernel not to accept any packets from devices
that are not directly connected to us (`scope = 'Link'`).

```sh
$ ip address show dev wlan0
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    …
    inet6 fe90::223:14ff:feb9:384/64 scope link dynamic wlan0
       valid_lft 25sec preferred_lft 25sec
    …
```

### Querying all IP addresses ###

Check out the [`print_all` example](example-print-all.html).

## API documentation ##

```eval_rst
.. autoclass:: netlink.Network
    :members:

.. autoclass:: netlink.NetworkDevice
    :members:

```
