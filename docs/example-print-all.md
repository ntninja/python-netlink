```eval_rst
.. _example-print-all:
```

# Example: Showing all network device related information #

Basic example on how to print all interface and address information of a network device.

## Sample output ##

```

Interface "lo" (1):
 • MTU: ................ 65536
 • FLAGS: .............. ['LOWER_UP', 'RUNNING', 'UP', 'LOOPBACK']
 • ADDRESS: ............ 00:00:00:00:00:00
 • PROMISCUITY: ........ 1
 • TYPE: ............... LOOPBACK
 • PROTINFO: ........... DISABLED
 • BROADCAST: .......... 00:00:00:00:00:00
 • INDEX: .............. 1
 • OPERSTATE: .......... UNKNOWN
 • AF_SPEC: ............ 0
 • EXT_MASK: ........... []
 • FAMILY: ............. 0
 • WEIGHT: ............. 0
 • NUM_TX_QUEUES: ...... 1
 • QDISC: .............. noqueue
 • PHYS_PORT_ID: ....... 0
 • IFNAME: ............. lo
 • NUM_RX_QUEUES: ...... 1
 • PHYS_PORT_NAME: ..... 
Address "127.0.0.1":
 • FLAGS: .............. ['PERMANENT']
 • CACHEINFO: .......... {'preferred': 4294967295, 'cstamp': 602, 'tstamp': 1407340, 'valid': 4294967295}
 • LABEL: .............. lo
 • SCOPE: .............. HOST
 • FAMILY: ............. 2
 • ADDRESS: ............ 127.0.0.1
 • INDEX: .............. 1
 • LOCAL: .............. 127.0.0.1
 • PREFIXLEN: .......... 8
Address "::1":
 • FAMILY: ............. 10
 • SCOPE: .............. HOST
 • CACHEINFO: .......... {'preferred': 4294967295, 'cstamp': 602, 'tstamp': 602, 'valid': 4294967295}
 • PREFIXLEN: .......... 128
 • FLAGS: .............. ['PERMANENT']
 • INDEX: .............. 1
 • ADDRESS: ............ ::1


Interface "eth0" (2):
 • MTU: ................ 1500
 • FLAGS: .............. ['MULTICAST', 'UP', 'BROADCAST']
 • ADDRESS: ............ 54:42:49:5C:BC:23
 • PROMISCUITY: ........ 1
 • TYPE: ............... ETHER
 • PROTINFO: ........... 1000
 • BROADCAST: .......... FF:FF:FF:FF:FF:FF
 • INDEX: .............. 2
 • OPERSTATE: .......... UNKNOWN
 • AF_SPEC: ............ 0
 • EXT_MASK: ........... []
 • FAMILY: ............. 0
 • WEIGHT: ............. 2
 • NUM_TX_QUEUES: ...... 1
 • QDISC: .............. pfifo_fast
 • PHYS_PORT_ID: ....... 1
 • IFNAME: ............. eth0
 • NUM_RX_QUEUES: ...... 0
 • PHYS_PORT_NAME: ..... 
Statistics:
 …

```

## Source code ##

```eval_rst
.. literalinclude:: ../examples/print_all.py
```
