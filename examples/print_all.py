#!/usr/bin/python3
import netlink

with netlink.Network() as network:
	for device, attributes in network.get_interfaces_with_addresses().items():
		# Print header
		print()
		print("Interface \"{0}\" ({1}):".format(device.name, device.index))
		
		# Move address list out of the general interface attribute list
		addresses = attributes["ADDRESSES"]
		del attributes["ADDRESSES"]
		
		# Print interface attributes
		for name, value in attributes.items():
			space = '.' * (16 - len(str(name)))
			print(" • {0}: ...{2} {1}".format(name, value, space))
		
		# Print interface addresses attributes
		for address, properties in addresses.items():
			# Print address header
			print("Address \"{0}\":".format(address))
			
			# Print address attributes
			for name, value in properties.items():
				space = '.' * (16 - len(str(name)))
				print(" • {0}: ...{2} {1}".format(name, value, space))
		
		# Print statistical information (only if the list is non-empty)
		statistics = device.get_statistics()
		if statistics:
			# Print statistics header
			print("Statistics:")
			
			# Print statistical attributes
			for name, value in statistics.items():
				space = '.' * (32 - len(str(name)))
				print(" • {0}: ...{2} {1}".format(name, value, space))
		
		# Print footer
		print()
