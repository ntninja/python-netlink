 * Document `get_link_information`

 * Document the low-level API
 * Implement support for all [`_BROKEN_LINK_ATTRIBUTES`](netlink/__init__.py#L236) in `netlink.low.netlinkattrib`
 * Implement `add_link`, `modify_link` and `remove_link`
 * Implement everything else the `ip` command can do
 * Implement everything else the `ethtool` command can do
