#!/usr/bin/python3
import os
import sys
import time
import unittest
import warnings

import netlink


def get_caps():
	try:
		with open("/proc/self/status", "r") as proc_status:
			for proc_status_line in proc_status:
				name, value = proc_status_line.split(':', 1)
				if name.strip() == "CapPrm":
					return int(value.strip(), 16)
	except:
		pass
	
	# Assume we have all possible capabilities if we can't determine the actual value
	return 0xFFFFFFFFFFFFFFFF


class QueryAll(unittest.TestCase):
	"""
	Abuse the ``print_all`` example code to check whether it's possible to query all interfaces
	for their data successfully
	"""
	def test_query_all(self):
		with open(os.devnull, 'w') as devnull:
			try:
				# Prevent the use of `print()`
				sys.stdout = devnull
				
				import examples.print_all
			finally:
				sys.stdout = sys.__stdout__


class AddressAddRemove(unittest.TestCase):
	skipUnlessRoot = unittest.skipUnless(get_caps() & (1 << 12),
								"Process does have the `CAP_NET_ADMIN` capability (`root` access)")
	
	TEST_INTERFACE = "lo"
	TEST_ADDRESS_1 = "127.1.1.1"
	TEST_ADDRESS_2 = "::25"
	
	def setUp(self):
		# Show warnings if tests run with too few permissions
		skipResult = self.__class__.skipUnlessRoot(None)
		if hasattr(skipResult, "__unittest_skip__") and skipResult.__unittest_skip__:
			message = "Address insertion and removal tests will be skipped"
			if hasattr(skipResult, "__unittest_skip_why__"):
				message = "{0}: {1}".format(message, skipResult.__unittest_skip_why__)
			warnings.warn(message)
		
		# Open the network loopback interface
		self.network = netlink.Network()
		self.device  = self.network.device(self.TEST_INTERFACE)
	
	def tearDown(self):
		self.network.close()
	
	def test_01_query(self):
		# Retrieve all addresses (warm up test)
		self.__class__.addresses = self.device.get_addresses()
		
		#XXX: Also retrieve interface here while there is no `InterfaceAddRemove` test
		self.device.get_interface()
	
	@skipUnlessRoot
	def test_02_add_address(self):
		# Add new IP address
		self.device.add_address(self.TEST_ADDRESS_1, scope = "SITE")
		
		# Verify that the address was added
		addresses = self.device.get_addresses()
		self.assertIn(self.TEST_ADDRESS_1, addresses)
		self.assertEqual(addresses[self.TEST_ADDRESS_1]['SCOPE'].upper(), "SITE")
		self.assertEqual(addresses[self.TEST_ADDRESS_1]['LABEL'], self.TEST_INTERFACE)
	
	@skipUnlessRoot
	def test_03_modify_address(self):
		# Change the IP address' label added in `test_02_add_address`
		self.device.modify_address(self.TEST_ADDRESS_1, preferred_lft = 200, valid_lft = 400)
		
		# Verify that the address has a new label
		addresses = self.device.get_addresses()
		self.assertIn(self.TEST_ADDRESS_1, addresses)
		self.assertEqual(addresses[self.TEST_ADDRESS_1]['CACHEINFO']['preferred'], 200)
		self.assertEqual(addresses[self.TEST_ADDRESS_1]['CACHEINFO']['valid'],     400)
	
	@skipUnlessRoot
	def test_04_remove_address(self):
		# Store list of IP addresses
		addresses = self.device.get_addresses()
		self.assertNotEqual(addresses, self.__class__.addresses)
		
		# Remove address
		self.device.remove_address(self.TEST_ADDRESS_1)
		
		# Verify that the list of IP addresses has changed, but is not the same as it was back when
		# `test_01_query` was called
		addresses2 = self.device.get_addresses()
		self.assertNotIn(self.TEST_ADDRESS_1, addresses2)
		self.assertNotEqual(addresses, addresses2)
		self.assertEqual(addresses2, self.__class__.addresses)
	
	@skipUnlessRoot
	def test_05_address_expiry(self):
		# Add an IP address with a lifetime of exactly one second
		self.device.add_address(self.TEST_ADDRESS_2, preferred_lft = 1, valid_lft = 1)
		
		# Verify that the address was added
		addresses = self.device.get_addresses()
		self.assertIn(self.TEST_ADDRESS_2, addresses)
		
		# Wait for the address to expire
		# Address expiry counting actually only starts the next whole second, so we have to wait
		# two seconds: Up to almost one second until the counter starts and one until the address
		# actually expires
		time.sleep(2)
		
		# Verify that the address is gone now
		addresses = self.device.get_addresses()
		self.assertNotIn(self.TEST_ADDRESS_2, addresses)
	


if __name__ == '__main__':
	unittest.main()