# Local install #

```
python3 setup.py build
sudo python3 setup.py install
```

# Distribution #

 1. Install `pandoc` (`sudo aptitude install pandoc`)
 2. Install the python `setuptools-markdown` module (`sudo pip3 install setuptools-markdown`)
 3. Run build as usual: `python3 bdist_wheel`

Build will result in the file `dist/NetLink-*-py3-none-any.whl` being created.

# Documentation #

 1. Install the python `sphinx.ext.napoleon` and `recommonmark` modules (`sudo pip3 install sphinxcontrib-napoleon recommonmark==0.4.0`)
 2. Run the sphinx HTML generator: `make -C docs/ html`

Documentation will be placed in `build/sphinx/html/`.

# Tests #

 1. Run `./tests.py` as *root*
   - Without *root* access, no all tests that would attempt to change any network settings will run.
